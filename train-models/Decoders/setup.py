from setuptools import setup, find_packages

setup(
    name='trainer',
    version='0.1',
    author='Tim Machado',
    packages=find_packages(),
    install_requires=[
        'keras',
        'h5py',
        'pandas'
    ],
    include_package_data=True,
    zip_safe=False
)
