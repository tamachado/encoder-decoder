export PROJECT_ID=$(gcloud config list project --format "value(core.project)")
export TRAIN_BUCKET="gs://${PROJECT_ID}-keras" # Staging directory
export JOB_DIR="gs://${PROJECT_ID}-decoders" # Directory to save output to
export REGION="us-east1" # You must use a region containing GPUs
export CONFIG="trainer/config_gpu.yaml" # Cluster configuration

# Train three AR2 models and two AR1 models.
export JOB_ID="${USER}_$(date +%Y%m%d_%H%M%S)"
gcloud ml-engine jobs submit training ${JOB_ID} \
    --package-path trainer \
    --module-name trainer.train_decoder \
    --staging-bucket "${TRAIN_BUCKET}" \
    --region ${REGION} \
    --config ${CONFIG} \
    -- \
    --num_traces 5000 \
    --epochs 20 \
    --units 100 \
    --dt .04 \
    --layer_dilation 1 2 4 8 \
    --width 32 \
    --baseline \
    --random_jump \
    --ar 2 \
    --rate  0 2 \
    --sigma 0 1 \
    --gamma .9 .9991 \
    --job_dir ${JOB_DIR}

sleep 1

export JOB_ID="${USER}_$(date +%Y%m%d_%H%M%S)"
gcloud ml-engine jobs submit training ${JOB_ID} \
    --package-path trainer \
    --module-name trainer.train_decoder \
    --staging-bucket "${TRAIN_BUCKET}" \
    --region ${REGION} \
    --config ${CONFIG} \
    -- \
    --num_traces 5000 \
    --epochs 20 \
    --units 100 \
    --dt .04 \
    --layer_dilation 1 2 4 8 \
    --width 32 \
    --baseline \
    --ar 2 \
    --rate  0 2 \
    --sigma 0 1 \
    --gamma .9 .9991 \
    --job_dir ${JOB_DIR}

sleep 1

export JOB_ID="${USER}_$(date +%Y%m%d_%H%M%S)"
gcloud ml-engine jobs submit training ${JOB_ID} \
    --package-path trainer \
    --module-name trainer.train_decoder \
    --staging-bucket "${TRAIN_BUCKET}" \
    --region ${REGION} \
    --config ${CONFIG} \
    -- \
    --num_traces 5000 \
    --epochs 20 \
    --units 100 \
    --dt .01 \
    --layer_dilation 1 2 4 8 \
    --width 32 \
    --baseline \
    --random_jump \
    --ar 2 \
    --rate  0 2 \
    --sigma 0.5 3 \
    --gamma .906 .997 \
    --job_dir ${JOB_DIR}

sleep 1

export JOB_ID="${USER}_$(date +%Y%m%d_%H%M%S)"
gcloud ml-engine jobs submit training ${JOB_ID} \
    --package-path trainer \
    --module-name trainer.train_decoder \
    --staging-bucket "${TRAIN_BUCKET}" \
    --region ${REGION} \
    --config ${CONFIG} \
    -- \
    --num_traces 5000 \
    --epochs 20 \
    --units 100 \
    --dt .01 \
    --layer_dilation 1 2 4 8 \
    --width 32 \
    --baseline \
    --random_jump \
    --ar 1 \
    --rate  0 2 \
    --sigma 0.5 3 \
    --gamma .906 .997 \
    --job_dir ${JOB_DIR}
