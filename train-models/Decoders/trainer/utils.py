from collections import defaultdict
from scipy import signal
import numpy as np
import scipy


def axcov(data, maxlag=5):
    """ From OASIS. """
    data = data - np.mean(data)
    T = len(data)
    exponent = 0
    while 2 * T - 1 > np.power(2, exponent):
        exponent += 1
    xcov = np.fft.fft(data, np.power(2, exponent))
    xcov = np.fft.ifft(np.square(np.abs(xcov)))
    xcov = np.concatenate([xcov[np.arange(xcov.size - maxlag, xcov.size)],
                           xcov[np.arange(0, maxlag + 1)]])
    return np.real(xcov / T)


def estimate_noise_scale(y, range_ff=[0.25, 0.5], method='mean'):
    """ From OASIS. """
    ff, Pxx = signal.welch(y)
    ind1 = ff > range_ff[0]
    ind2 = ff < range_ff[1]
    ind = np.logical_and(ind1, ind2)
    Pxx_ind = Pxx[ind]
    sn = lambda Pxx_ind: np.sqrt(np.mean(Pxx_ind / 2))
    sn = sn(Pxx_ind)
    return sn


def estimate_time_constant(y, p, sn, lags=5, fudge_factor=1.):
    """ From OASIS. """
    lags += p
    xc = axcov(y, lags)
    xc = xc[:, np.newaxis]

    l_i = np.eye(lags, p)
    A = scipy.linalg.toeplitz(xc[lags + np.arange(lags)],
                              xc[lags + np.arange(p)]) - sn**2 * l_i
    g = np.linalg.lstsq(A, xc[lags + 1:])[0]
    gr = np.roots(np.concatenate([np.array([1]), -g.flatten()]))
    gr = (gr + gr.conjugate()) / 2.
    gr[gr > 1] = 0.95 + np.random.normal(0, 0.01, np.sum(gr > 1))
    gr[gr < 0] = 0.15 + np.random.normal(0, 0.01, np.sum(gr < 0))
    g = np.poly(fudge_factor * gr)
    g = -g[1:]

    return g.flatten()


def downsample(signal, factor):
    """ Downsample signal by factor. From C2S. """
    if factor < 2:
        return np.asarray(signal)
    return np.convolve(np.asarray(signal).ravel(),
                       np.ones(factor), 'valid')[::factor]


def baseline_drift(T, scale_factor=20, band=0.001, pad=1000):
    """ Low-pass filter white noise to get a simulated baseline term. """
    x = np.random.randn(T+2*pad) * scale_factor * np.random.random()
    b, a = signal.butter(3, band)
    zi = signal.lfilter_zi(b, a)
    z, _ = signal.lfilter(b, a, x, zi=zi*x[0])
    z = z[pad:pad+T]
    z = z - min(z)
    return z


def fit_C(spikes, ar, gamma, random_jump=False):
    """ Generate noiseless calcium given spikes and a gamma/decay range. """

    # Generate a random gamma
    scale = 100
    gamma = (np.log(np.random.uniform(np.exp(gamma[0]*scale),
             np.exp(gamma[1]*scale)))/scale)

    # Compute C given spikes and gamma
    C = spikes.astype(float)

    # Randomize the jump size
    if random_jump:
        for i in range(len(C)):
            C[i] = C[i] * np.random.random() + C[i]*.5

    # Use an AR(1) or an AR(2) model
    if ar == 1:
        for i in range(2, len(C)):
            C[i] += gamma * C[i - 1]
    else:
        a = np.random.random() * .5 + 1
        b = gamma - a
        for i in range(2, len(C)):
            C[i] += a * C[i - 1] + b * C[i - 2]

        # Get the response to a single spike
        p = np.zeros(300)
        p[150] = 1
        for i in range(2, len(p)):
            p[i] += a * p[i - 1] + b * p[i - 2]

        # Normalize the peaks of the unitary spike response
        C = C/max(p)
    return C, gamma


def make_data_stack(**kwargs):
    """ Append noise estimates to generated fluorescence data. """
    p = kwargs['ar']
    data = make_data(**kwargs)
    n_cells = np.shape(data['F'])[0]
    n_time = np.shape(data['F'])[1]
    sn = np.ones((n_cells, n_time))
    g1 = np.ones((n_cells, n_time))
    g2 = np.ones((n_cells, n_time))
    for ind in range(n_cells):
        F = np.squeeze(data['F'][ind, :])
        gam_ = estimate_time_constant(F, p, sn[ind, 0])
        g1[ind, :] = gam_[0]
        if p > 1:
            g2[ind, :] = gam_[1]

    stack = (np.stack([data['F'][:, :, 0], sn, g1, g2], axis=2) if p > 1 else
             np.stack([data['F'][:, :, 0], sn, g1], axis=2))
    return data, stack


def make_data(dt=1/25, duration=1000, num_traces=100, ar=1,
              sigma=[.1, .4], gamma=[.92, .98], rate=[0, 2],
              seed=11111, baseline_band=.001, baseline_scale=1,
              baseline=True, random_jump=False, **kwargs):
    """
    Generate simulated calcium fluorescence data with poisson spiking

    Parameters
    ----------
    dt : 1 / framerate (in seconds)
    duration : float, optional, default 10000
        Number of timesteps in each simulated trace.
    num_traces : float, optional, default 100
        Approximate number of traces to generate.
    sigma : list, optional, default [.1 0.4]
        Noise scale range.
    gamma : list, optional, default [.92 .98]
        Decay scale range.
    rate : int, optional, default [0 2]
        Max firing rate (in Hz) of simulated spike trains.
    baseline : bool, optional, default True
        Use a drifting baseline term, or just a scalar offset.
    baseline_scale : float, optional, default 1
        Scale factor on baseline drift term (if enabled).
    baseline_band : float, optional, default .001
        Cutoff frequency (multiple of Nyquist)
        for low pass filter to estimate baseline.
    random_jump : bool, optional, default False
        Randomize the jump amplitude following each spike.
    seed : int, optional, default 11111
        Seed of random number generator.

    Returns
    -------
    A dict with the following fields:
    F : array, shape (N, T, 1)
        Noisy fluorescence data.
    s : array, shape (N, T, 1)
        Simulated spike trains.
    C : array, shape (N, T, 1)
        Noiseless simulated calcium.
    g : list, length N
        Decay values chosen for each neuron.
    sn : list, length N
        Noise values chosen for each neuron.
    """

    np.random.seed(seed)
    data = defaultdict(list)

    for c in range(num_traces):

        # Generate and save some random parameters
        rate_ = np.random.random()*(rate[1]-rate[0]) + rate[0]
        sigma_ = np.random.random()*(sigma[1]-sigma[0]) + sigma[0]
        data['sn'].append(sigma_)

        # Generate random spike train
        spike_dt = dt
        spike_duration = duration
        s_ = np.random.poisson(rate_ / float(1/spike_dt), spike_duration)

        # Generate simulated calcium with an AR(p) model
        C_, g_ = fit_C(s_, ar, gamma, random_jump)
        data['gam'].append(g_)

        # Use a baseline or not
        if baseline:
            b = baseline_drift(duration, band=baseline_band) * baseline_scale
        else:
            b = np.random.random()

        # Add gaussian noise
        sn_ = sigma_ * np.random.randn(1, duration)

        # Save results
        data['F'].append(C_ + sn_ + b)
        data['C'].append(C_)
        data['s'].append(s_)

    data['F'] = np.reshape(data['F'], (num_traces, -1, 1))
    data['C'] = np.reshape(data['C'], (num_traces, -1, 1))
    data['s'] = np.reshape(data['s'], (num_traces, -1, 1))

    return data
