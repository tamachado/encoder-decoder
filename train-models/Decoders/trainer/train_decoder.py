from __future__ import division

from collections import defaultdict
from datetime import datetime
import argparse

from tensorflow.python.lib.io import file_io
import tensorflow as tf
sess = tf.Session()

from keras import backend as K
K.set_session(sess)

import keras
import utils


def make_decoder(width=16, units=100, seed=1111, ar=1,
                 layer_dilation=[8, 4, 2, 1], dropout=0.5, **kwargs):
    " Build NN decoder model to train using parameters specified. "
    F = keras.layers.Input(shape=(None, 2+ar))
    x = F
    for d in layer_dilation:
        x = keras.layers.Conv1D(units, width, dilation_rate=d,
                                activation='relu', padding='same')(x)
        x = keras.layers.Dropout(dropout, seed=seed)(x)
    s = keras.layers.Conv1D(1, 1, activation='relu', padding='same')(x)
    decoder = keras.models.Model(inputs=F, outputs=s)
    decoder.compile(optimizer='adam', metrics=['accuracy'],
                    loss='binary_crossentropy')

    return decoder


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Train a decoding model for spike inference',
    )

    parser.add_argument('--job_dir', default='models')
    parser.add_argument('--num_traces', type=int, default=50)
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--units', type=int, default=100)
    parser.add_argument('--width', type=int, default=16)
    parser.add_argument('--ar', type=int, default=1)
    parser.add_argument('--dt', type=float, default=0.04)
    parser.add_argument('--rate', nargs='+', type=float, default=[0, 1.5])
    parser.add_argument('--sigma', nargs='+', type=float, default=[0, 1])
    parser.add_argument('--gamma', nargs='+', type=float, default=[.93, .98])
    parser.add_argument('--layer_dilation', nargs='+',
                        type=int, default=[8, 4, 2, 1])
    parser.add_argument('--baseline_scale', type=float, default=1)
    parser.add_argument('--baseline', dest='baseline', action='store_true')
    parser.add_argument('--no-baseline', dest='baseline', action='store_false')
    parser.add_argument('--save-data', dest='save_data', action='store_true')
    parser.add_argument('--random_jump', dest='random_jump',
                        action='store_true')
    parser.set_defaults(random_jump=False)
    parser.set_defaults(baseline=False)
    args = parser.parse_args()

    # Print name based on hyperparameters
    nn = (str(args.num_traces) + '-' + str(args.epochs) + '-' +
          str(args.rate) + '-' + str(args.sigma) +
          '-' + str(args.baseline) + '-' +
          str(args.gamma) + '-' + str(args.baseline_scale) +
          '-' + str(args.units) + '-' + str(args.layer_dilation) +
          '-' + str(args.random_jump) +
          '-' + str(args.width) + '-' + str(args.dt) + '-' + str(args.ar))
    name = nn + '.hdf5'
    print('Name: ', name)
    print('Job dir = ' + args.job_dir)
    job_dir = args.job_dir
    log_path = job_dir + '/logs/' + datetime.now().isoformat()

    p = defaultdict()

    # Training parameters
    p['num_traces'] = args.num_traces
    epochs = args.epochs
    split = 0.1  # Validation split

    # Set up the hyperparameters for the data generator
    p['rate'] = args.rate
    p['sigma'] = args.sigma
    p['baseline'] = args.baseline
    p['baseline_scale'] = args.baseline_scale
    p['random_jump'] = args.random_jump
    p['gamma'] = args.gamma
    p['ar'] = args.ar
    p['dt'] = args.dt
    p['baseline_band'] = 0.001
    p['duration'] = 3000

    # Network parameters
    p['seed'] = 11111  # This is the RNG seed used for dropout and data
    p['layer_dilation'] = args.layer_dilation  # Determines number of layers
    p['units'] = args.units  # This is the number of units in each layer
    p['width'] = args.width  # This is the width all convolutional filters

    # Copy the parameters with a different seed to make test data
    pt = p.copy()
    pt['seed'] = pt['seed'] * 555
    pt['num_traces'] = 1000

    # Make some data (add noise only)
    train_data, train_stack = utils.make_data_stack(**p)
    test_data, test_stack = utils.make_data_stack(**pt)

    # Make a model
    decoder = make_decoder(**p)

    # Fit it
    checkpoint = keras.callbacks.ModelCheckpoint(name, monitor='val_loss',
                                                 save_best_only=True)
    history = decoder.fit(train_stack, train_data['s'],
                          epochs=epochs, verbose=1, validation_split=split,
                          callbacks=[checkpoint])

    # Load the best model
    decoder = keras.models.load_model(name)

    # Evaluate the model
    score = decoder.evaluate(test_stack, test_data['s'], verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    # Move the best model to our storage bucket
    try:
        out_path = job_dir + '/' + name
        with file_io.FileIO(name, mode='r') as input_f:
            with file_io.FileIO(out_path, mode='w+') as output_f:
                output_f.write(input_f.read())
        print('Moved trained model to output bucket.')
    except:
        pass
