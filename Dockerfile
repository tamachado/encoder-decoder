################################################################################
# Dockerfile based on github.com/tensorflow/tensorflow/blob/...
#   master/tensorflow/tools/docker/Dockerfile
################################################################################

FROM ubuntu:16.04

MAINTAINER Tim Machado "tamachado@stanford.edu"

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        python \
        python-dev \
        python-pip \
        python-distribute \
        rsync \
        software-properties-common \
        unzip \
        git \
        python-pip \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install \
        'ipython<6' \
        jupyter \
        matplotlib \
        numpy \
        scipy \
        sklearn \
        pandas \
        Pillow \
        keras \
        seaborn \
        h5py \
        tensorflow

# Install spikefinder library
RUN git clone https://github.com/codeneuro/spikefinder-python.git /spikefinder
RUN ls /spikefinder
RUN cd spikefinder && python setup.py install

# Install encoder-decoder
RUN git clone https://bitbucket.org/tamachado/encoder-decoder.git
