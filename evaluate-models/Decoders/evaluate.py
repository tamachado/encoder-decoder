"""
 This script will load up the models previously trained and placed in the
 Models subdirectory. It will then evaluate each model on each
 training dataset. See 'Decoder Model Evaluation.ipynb' to regenerate CSV
 files for submission.

 tamachado@stanford.edu
"""
import os
import keras
import numpy as np

import pandas as pd
import utils

# Set up parameters
DECODER_FOLDER = 'Models'  # This folder contains the decoder NN models.
HIGH_DS = [6, 7, 8, 9, 10]  # Process datasets at 100 Hz
FACTORS = [7, 7, 7, 7, 7, 2, 2, 2, 7, 4]  # Decimate data to estimate noise
COVARIATES = True  # Pass estimates of decay + noise into decoder?
SCALE = 5  # Gaussian filter parameter for dataset 5
SIZE = 13  # Gaussian filter parameter for dataset 5

if __name__ == "__main__":

    # Load the models to evaluate
    print('Loading models.')
    models = []
    current_path = os.path.dirname(os.path.abspath(__file__))
    names = os.listdir(current_path + os.path.sep + DECODER_FOLDER)
    [names.remove(name) for name in names if name[0] == '.']
    ar = []
    for x in names:
        if x[0] is not '.':
            model = (current_path + os.path.sep +
                     DECODER_FOLDER + os.path.sep + x)
            models.append(keras.models.load_model(model))

            # Get the AR model type (1 or 2) from the filename
            ar.append(int(x.split('-')[utils.AR_IDX+1][0]))

    results = []  # Store all the summary dataframes
    spikes_all = []  # Store all the CSV files from all the training data
    spikes_all_test = []  # Store all the CSV files from all the test data

    # Evaluate all datasets
    for ds in range(1, 11):
        print('Evaluating dataset ' + str(ds))
        F, s = utils.load_evaluation_data(ds)
        hr = True if ds in HIGH_DS else False

        # If we're on dataset 5 convolve with a gaussian
        # (we optimized these parameters elsewhere):
        if ds == 5:
            kernel_scale = SCALE
            kernel_size = SIZE
        else:
            kernel_scale = None
            kernel_size = None

        # Evaluate each dataset
        result, spikes = utils.evaluate_dataset(F, s, np.array(models),
                                                np.array(names), p=ar,
                                                factor=FACTORS[ds-1],
                                                high_resolution=hr,
                                                kernel_scale=kernel_scale,
                                                kernel_size=kernel_size,
                                                covariates=COVARIATES,
                                                dataset=ds)
        results.append(result)
        spikes_all.append(spikes)

    results_all = pd.concat(results)

    # Get the model that performed best for each dataset
    # (mean=False gives models that maximize the median)
    best_models, best_corr = utils.get_best_models(results_all, names,
                                                   display=False,
                                                   plot=False, mean=False)
    print('Correlation for each dataset: ', np.round(best_corr, 4))
