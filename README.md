This repo contains our Encoder-Decoder spike inference framework. It also contains a Dockerfile that you can build that will install the minimum dependencies to reproduce these results.

The results generated from training new models will be similar to, but not exactly the same as, the results submitted to the contest, due to small changes to the codebase and reproducibility issues with Tensorflow.

This code was written by Tim Machado, in collaboration with Liam Paninski, for the Spikefinder contest.

# Getting Started #
To start see `encoder-decoders/evaluate-models/Decoders/Basic Usage Case.ipynb`

# 1. Build the docker image: #
* Install docker.
* Build the image by executing `docker build -t \"encoder-decoder:latest\" .` from the root repo directory.
* Run the image with `docker run -it encoder-decoder`

# 2. Train new models (optional): #

* Install Google Cloud Platform tools and run `source /encoder-decoder/train-models/Decoders/train_models.sh`. Since you need to make your own Google Cloud account to do this anyway, the Google Cloud tools are not installed in the docker image.
* Alternatively, you can use the parameters specified in `train_models.sh` and locally run `python /encoder-decoder/train-models/Decoders/trainer/train_decoder.py` to generate individual models.
* Either approach will use our analytical generative model of calcium data (the encoder) to make training data, and then proceed to train a convolutional neural network (the decoder) on that data.

# 3. Evaluate existing models: #
* For a minimal test that will run all available decoding models on all the test dataset, run `python /encoder-decoder/evaluate-models/Decoders/evaluate.py`
* Also please look at `/encoder-decoder/evaluate-models/Decoders/Decoder Model Evaluation.ipynb` for more details. That notebook will also generate and save the CSV files containing our inference results.
